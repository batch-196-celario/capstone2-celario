const Order = require("../Models/Order");


module.exports.createOrders = (req, res) => {
  
	if(req.user.isAdmin){
 		return res.send({message: "Action Forbidden."})
 	} 
 	else {

 		let newOrder = new Order({
 			userId: req.user.id,
 			products: req.body.products,
 			totalAmt: req.body.totalAmt
 		});
 		newOrder.save()
 		.then(result => res.send({message: "Order Successful"}))
 		.catch(error => res.send(error))
 	}
}

module.exports.allUserOrders = (req, res) => {
 		

   Order.find({})
  .then(result => res.send(result))
  .catch(error => res.send(error))
  
}


module.exports.getUserOrders = (req, res) => {
 		

   Order.find({userId:req.user.id})
  .then(result => res.send(result))
  .catch(error => res.send(error))
  
}

module.exports.displayProducts = (req, res) => {
 		

   Order.find({},{_id:0,userId:0,totalAmt:0,purchasedOn:0})
  .then(result => res.send(result))
  .catch(error => res.send(error))
  
}