const User = require("../Models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
//const Product = require("../models/Product")

module.exports.registerUser = (req,res)=>{


	//console.log(req.body)
	const hashedPw = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPw);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo


	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.loginUser = (req,res) => {

	console.log(req.body);

	User.findOne({email:req.body.email})
	.then(foundUser => {

		if(foundUser === null){
			return res.send({message: "No User Found."})
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			}else {
				return res.send({message: "Incorrect Password"})
			}
		}
	
	})

}


module.exports.getUserAuthentication = (req, res) => {
 	
   User.findById(req.user.id)
  .then(result => res.send(result))
  .catch(error => res.send(error))
  
}


module.exports.setUserAdmin = (req,res) => {

  let update = {
    isAdmin: true
  }

  User.findByIdAndUpdate(req.params.userId,update,{new:true})
  .then(result => res.send(result))
  .catch(error => res.send(error))
}





// module.exports.checkOut = async (req,res) => {
	
// 	if(req.user.isAdmin){
// 		return res.send({message: "Action Forbidden."})
// 	} 

// 	let isUserUpdated = await User.findById(req.user.id).then(user =>{

// 			console.log(user);
// 			 let newOrder = [
// 			 	{ 
// 						productId: req.body.productId,
// 			 		quantity: req.body.quantity
			 		
// 			 	},
// 			    {
// 			 		totalAmount: req.body.totalAmount
// 			 	}
// 			 ]

// 			 user.orders.forEach(newOrder)


// 			 return user.save().then(user => true).catch(err => err.message)

// 	})

// 	if (isUserUpdated !== true) {
// 		return res.send({message: isUserUpdated})
// 	}

// 	//console.log(isUserUpdated);

// 	let isProductUpdated = await Product.findById(req.body.productId).then(Product => {


// 			let userCheckOut = {
// 				userId: req.user.id
// 			}

// 			product.orders.push(userCheckOut);

// 			return course.save().then(Product => true).catch(err => err.message);

// 	})

// 	//console.log(isCourseUpdated);

// 	if(isProductUpdated !== true){
// 		return res.send({message: isCourseUpdated})
// 	}


// 	if(isUserUpdated && isProductUpdated){
// 		return res.send({message: "Thank you for Buying!"})
// 	}

// }
