const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers")
const auth = require("../auth");



const {verify,verifyAdmin} = auth;


router.post("/",userControllers.registerUser);

router.post('/login',userControllers.loginUser)

router.get('/userDetails',verify,userControllers.getUserAuthentication);

router.put('/setUser/:userId',verify,verifyAdmin,userControllers.setUserAdmin);

module.exports = router;
