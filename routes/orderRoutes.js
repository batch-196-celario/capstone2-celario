const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers");
const auth = require("../auth");

const { verify,verifyAdmin } = auth;

router.get('/',verify,verifyAdmin,orderControllers.allUserOrders);

router.post('/createOrder',verify,orderControllers.createOrders);

router.get('/getUserOrder',verify,orderControllers.getUserOrders);

router.get('/display',orderControllers.displayProducts);

module.exports = router;