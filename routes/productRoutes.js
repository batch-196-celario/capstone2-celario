const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

const { verify,verifyAdmin } = auth;

router.get('/',productControllers.getAllProducts);

router.get('/active',productControllers.getActiveProducts);

router.post('/createProduct',verify,verifyAdmin,productControllers.CreateProduct);

router.get('/SingleProduct/:productId',productControllers.SingleProduct);

router.put('/updateProduct/:productId',verify,verifyAdmin,productControllers.updateProduct);

router.delete("/archiveProduct/:productId",verify,verifyAdmin,productControllers.archiveProduct);

router.post("/activateProduct/:productId",verify,verifyAdmin,productControllers.activateProduct); 

module.exports = router;

